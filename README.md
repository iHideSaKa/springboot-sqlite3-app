# Springboot-Sqlite3-App

## How to initialize without Docker

### Build SERVER jar

To build the SERVER, to go to **springboot-sqlite3-app-server** folder and execute the following command:

```bash
mvn package
```

### Start SERVER application

Still on **springboot-sqlite3-app-server** folder, to start the SERVER execute the following command:

```bash
java -Dserver.port=8090 -jar target/springboot-sqlite3-app-server-0.0.1-SNAPSHOT.jar
```

### Install WEB dependecies

Then to initialize the WEB application, go to **springboot-sqlite3-app-web** folder and execute this

```bash
npm install
```

### Start WEB application

Still on **springboot-sqlite3-app-web** folder, to start the WEB execute the following command:

```bash
ng serve
```

## How to initialize using **Docker**

To run the application using Docker we need to create the Docker Image and Docker Container for both SERVER and WEB Applications.

### Create SERVER docker image

Go to **springboot-sqlite3-app-server** folder and execute the following command to create the docker image:

```bash
mvn clean install dockerfile:build
```

### Create and start SERVER docker container

Then execute the following command to create and start the docker container:

```bash
docker run --name springboot-sqlite3-app-server \
  -e 'SPRING_PROFILES_ACTIVE=docker' \
  -p 8090:8090 \
  -d springboot-sqlite3-app/springboot-sqlite3-app-server:0.0.1-SNAPSHOT
```

### Create WEB docker image

Go to **springboot-sqlite3-app-web** folder and execute the following command to create the docker image:

```bash
docker build -t springboot-sqlite3-app-web .
```

### Create and start WEB docker container

Then execute the following command to create and start the docker container:

```bash
docker run -it --name springboot-sqlite3-app-web\
  -v ${PWD}:/usr/src/app \
  -v /usr/src/app/node_modules \
  -p 4200:4200 \
  --rm \
  springboot-sqlite3-app-web:latest
```

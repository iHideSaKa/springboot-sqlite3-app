package com.ihidesaka.springbootsqlite3appserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.ihidesaka")
@EntityScan("com.ihidesaka.springbootsqlite3appserver.domain.entity")
public class SpringbootSqlite3AppServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSqlite3AppServerApplication.class, args);
    }

}

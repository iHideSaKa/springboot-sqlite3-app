package com.ihidesaka.springbootsqlite3appserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ihidesaka.springbootsqlite3appserver.service.CountryService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class CountryController {

    @Autowired
    CountryService countryService;

    @RequestMapping(path = "/countries", method = RequestMethod.GET)
    public ResponseEntity<?> getCountries() {
        return ResponseEntity.ok(countryService.getCountries());
    }

}

package com.ihidesaka.springbootsqlite3appserver.domain.enumerator;

import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Pattern;

public enum Country {
    CAMEROON("Cameroon", "+237", Pattern.compile("^\\(237\\)\\ ?[2368]\\d{7,8}$")),
    ETHIOPIA("Ethiopia", "+251", Pattern.compile("^\\(251\\)\\ ?[1-59]\\d{8}$")),
    MOROCCO("Morocco", "+212", Pattern.compile("^\\(212\\)\\ ?[5-9]\\d{8}$")),
    MOZAMBIQUE("Mozambique", "+258", Pattern.compile("^\\(258\\)\\ ?[28]\\d{7,8}$")),
    UGANDA("Uganda", "+256", Pattern.compile("^\\(256\\)\\ ?\\d{9}$"));

    Country(String countryName, String countryCode, Pattern phoneNumberPattern) {
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.phoneNumberPattern = phoneNumberPattern;
    }

    private final String countryName;

    private final String countryCode;

    private final Pattern phoneNumberPattern;

    public String getCountryName() {
        return this.countryName;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public Pattern getPhoneNumberPattern() {
        return this.phoneNumberPattern;
    }

    public static Optional<Country> phoneCodeOf(String countryCode) {
        return Arrays.stream(values()).filter(c -> countryCode.equals(c.countryCode)).findFirst();
    }

}

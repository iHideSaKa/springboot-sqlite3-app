package com.ihidesaka.springbootsqlite3appserver.mapper;

import com.ihidesaka.springbootsqlite3appserver.domain.enumerator.Country;
import com.ihidesaka.springbootsqlite3appserver.response.CountryResponse;

public interface CountryResponseMapper extends ReferenceMapper<Country, CountryResponse> {

}

package com.ihidesaka.springbootsqlite3appserver.mapper;

import com.ihidesaka.springbootsqlite3appserver.domain.entity.Customer;
import com.ihidesaka.springbootsqlite3appserver.response.CustomerResponse;

public interface CustomerResponseMapper extends ReferenceMapper<Customer, CustomerResponse> {

}

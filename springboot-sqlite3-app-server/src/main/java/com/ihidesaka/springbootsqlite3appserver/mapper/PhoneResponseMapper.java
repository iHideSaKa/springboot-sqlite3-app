package com.ihidesaka.springbootsqlite3appserver.mapper;

import com.ihidesaka.springbootsqlite3appserver.response.PhoneResponse;

public interface PhoneResponseMapper extends ReferenceMapper<String, PhoneResponse> {

}

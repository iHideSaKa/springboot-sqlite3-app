package com.ihidesaka.springbootsqlite3appserver.mapper;

public interface ReferenceMapper<TFrom, TTo> {
    TTo map(TFrom from);

}
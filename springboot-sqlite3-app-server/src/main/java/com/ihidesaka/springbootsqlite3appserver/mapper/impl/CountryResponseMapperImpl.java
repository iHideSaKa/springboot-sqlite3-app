package com.ihidesaka.springbootsqlite3appserver.mapper.impl;

import org.springframework.stereotype.Component;

import com.ihidesaka.springbootsqlite3appserver.domain.enumerator.Country;
import com.ihidesaka.springbootsqlite3appserver.mapper.CountryResponseMapper;
import com.ihidesaka.springbootsqlite3appserver.response.CountryResponse;

@Component
public class CountryResponseMapperImpl implements CountryResponseMapper {

    @Override
    public CountryResponse map(Country c) {
        CountryResponse country = new CountryResponse();
        country.setName(c.getCountryName());
        return country;
    }

}

package com.ihidesaka.springbootsqlite3appserver.mapper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ihidesaka.springbootsqlite3appserver.domain.entity.Customer;
import com.ihidesaka.springbootsqlite3appserver.mapper.CustomerResponseMapper;
import com.ihidesaka.springbootsqlite3appserver.mapper.PhoneResponseMapper;
import com.ihidesaka.springbootsqlite3appserver.response.CustomerResponse;

@Component
public class CustomerResponseMapperImpl implements CustomerResponseMapper {

    @Autowired
    private PhoneResponseMapper phoneResponseMapper;

    @Override
    public CustomerResponse map(Customer c) {
        CustomerResponse customer = new CustomerResponse();
        customer.setId(c.getId());
        customer.setName(c.getName());
        customer.setPhone(phoneResponseMapper.map(c.getPhone()));
        return customer;
    }

}

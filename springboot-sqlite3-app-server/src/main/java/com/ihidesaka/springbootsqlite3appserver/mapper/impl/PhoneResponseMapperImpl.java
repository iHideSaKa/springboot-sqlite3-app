package com.ihidesaka.springbootsqlite3appserver.mapper.impl;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.ihidesaka.springbootsqlite3appserver.domain.enumerator.Country;
import com.ihidesaka.springbootsqlite3appserver.mapper.PhoneResponseMapper;
import com.ihidesaka.springbootsqlite3appserver.response.PhoneResponse;

@Component
public class PhoneResponseMapperImpl implements PhoneResponseMapper {

    private static final String COUNTRY_CODE_PATTERN = "^\\([0-9]{1,3}\\)";
    private static final String COUNTRY_CODE_PATTERN_REPLACE = "(\\()([0-9]{1,3})(\\))";
    private static final String COUNTRY_CODE_PATTERN_REPLACE_GROUPS = "+$2";

    @Override
    public PhoneResponse map(String phoneNumber) {
        PhoneResponse phone = validateNumber(phoneNumber);
        phone.setNumber(phoneNumber);

        return phone;
    }

    private PhoneResponse validateNumber(String phoneNumber) {
        PhoneResponse phone = new PhoneResponse();

        // verifies if the phone has a valid country code pattern
        Pattern countryCodePattern = Pattern.compile(COUNTRY_CODE_PATTERN);
        Matcher countryCodeMatcher = countryCodePattern.matcher(phoneNumber);

        if (countryCodeMatcher.find()) {
            findCountryCodeWithinExistingCountryList(phoneNumber, phone, countryCodeMatcher);
        } else {
            setNumberInvalid(phone);
        }
        return phone;
    }

    private void findCountryCodeWithinExistingCountryList(String phoneNumber, PhoneResponse phone,
            Matcher countryCodeMatcher) {
        String countryCode = getCountryCode(countryCodeMatcher);
        Optional<Country> optionalCountry = Country.phoneCodeOf(countryCode);

        if (optionalCountry.isPresent()) {
            Country country = optionalCountry.get();
            phone.setCountryName(country.getCountryName());
            phone.setCountryCode(country.getCountryCode());

            // verifies if the phone pattern is valid for the corresponding country number
            // pattern
            Matcher phoneNumberMatcher = country.getPhoneNumberPattern().matcher(phoneNumber);
            phone.setState(phoneNumberMatcher.matches());
        } else {
            setNumberInvalid(phone);
        }
    }

    private String getCountryCode(Matcher countryCodeMatcher) {
        return countryCodeMatcher.group().replaceAll(COUNTRY_CODE_PATTERN_REPLACE, COUNTRY_CODE_PATTERN_REPLACE_GROUPS);
    }

    private void setNumberInvalid(PhoneResponse phone) {
        phone.setCountryName("");
        phone.setCountryCode("");
        phone.setState(false);
    }

}

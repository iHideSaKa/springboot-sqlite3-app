package com.ihidesaka.springbootsqlite3appserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ihidesaka.springbootsqlite3appserver.domain.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

//	@Query("select c from Customer c")
//    List<Customer> findAll();

}

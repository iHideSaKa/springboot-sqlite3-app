package com.ihidesaka.springbootsqlite3appserver.response;

public class CustomerResponse {
    private Integer id;
    private String name;
    private PhoneResponse phone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PhoneResponse getPhone() {
        return phone;
    }

    public void setPhone(PhoneResponse phone) {
        this.phone = phone;
    }

}

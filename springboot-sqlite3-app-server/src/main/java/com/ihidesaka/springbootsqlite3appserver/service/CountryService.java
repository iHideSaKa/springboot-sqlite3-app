package com.ihidesaka.springbootsqlite3appserver.service;

import java.util.List;

import com.ihidesaka.springbootsqlite3appserver.response.CountryResponse;

public interface CountryService {

    List<CountryResponse> getCountries();

}

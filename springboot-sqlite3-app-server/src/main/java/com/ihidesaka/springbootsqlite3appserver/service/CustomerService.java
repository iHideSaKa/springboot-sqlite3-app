package com.ihidesaka.springbootsqlite3appserver.service;

import java.util.List;

import com.ihidesaka.springbootsqlite3appserver.response.CustomerResponse;

public interface CustomerService {

    List<CustomerResponse> getCustomers();

}

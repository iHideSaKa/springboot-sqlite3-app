package com.ihidesaka.springbootsqlite3appserver.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ihidesaka.springbootsqlite3appserver.domain.enumerator.Country;
import com.ihidesaka.springbootsqlite3appserver.mapper.CountryResponseMapper;
import com.ihidesaka.springbootsqlite3appserver.response.CountryResponse;
import com.ihidesaka.springbootsqlite3appserver.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryResponseMapper countryResponseMapper;

    @Override
    public List<CountryResponse> getCountries() {
        return Arrays.stream(Country.values())
                .map(c -> countryResponseMapper.map(c))
                .collect(Collectors.toList());
    }

}

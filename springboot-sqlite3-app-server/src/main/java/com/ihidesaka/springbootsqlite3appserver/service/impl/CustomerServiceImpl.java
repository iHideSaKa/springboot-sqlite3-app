package com.ihidesaka.springbootsqlite3appserver.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ihidesaka.springbootsqlite3appserver.domain.entity.Customer;
import com.ihidesaka.springbootsqlite3appserver.mapper.CustomerResponseMapper;
import com.ihidesaka.springbootsqlite3appserver.repository.CustomerRepository;
import com.ihidesaka.springbootsqlite3appserver.response.CustomerResponse;
import com.ihidesaka.springbootsqlite3appserver.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerResponseMapper customerResponseMapper;

    @Override
    public List<CustomerResponse> getCustomers() {
        List<Customer> customerList = customerRepository.findAll();

        return customerList.stream()
                .map(c -> customerResponseMapper.map(c))
                .collect(Collectors.toList());
    }

}

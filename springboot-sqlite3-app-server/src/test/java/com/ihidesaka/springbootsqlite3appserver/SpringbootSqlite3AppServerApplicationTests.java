package com.ihidesaka.springbootsqlite3appserver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.ihidesaka.springbootsqlite3appserver.controller.CustomerController;
import com.ihidesaka.springbootsqlite3appserver.domain.entity.Customer;
import com.ihidesaka.springbootsqlite3appserver.domain.enumerator.Country;
import com.ihidesaka.springbootsqlite3appserver.mapper.PhoneResponseMapper;
import com.ihidesaka.springbootsqlite3appserver.repository.CustomerRepository;
import com.ihidesaka.springbootsqlite3appserver.response.CustomerResponse;
import com.ihidesaka.springbootsqlite3appserver.response.PhoneResponse;
import com.ihidesaka.springbootsqlite3appserver.utils.TestUtils;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-integration-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = {
        SpringbootSqlite3AppServerApplication.class })
public class SpringbootSqlite3AppServerApplicationTests {

    @Autowired
    private CustomerController customerController;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PhoneResponseMapper phoneResponseMapper;

    @Before
    public void initial() {
        deleteAllCustomers();
    }

    /**
     * GIVEN there exists zero {@link Customer}
     * WHEN a request for the list of all {@link Customer}
     * THEN an empty list should be returned.
     */
    @Test
    @Transactional
    public void shouldReturnEmptyList() {
        checkIfNoCustomer();
    }

    /**
     * GIVEN there exists one {@link Customer}
     * WHEN a request for the list of all {@link Customer}
     * THEN a list containing one {@link Customer} should be returned.
     */
    @Test
    @Transactional
    public void shouldReturnListWithOneCustomers() {
        checkIfNoCustomer();

        Customer newCustomer1 = createNewCustomer(TestUtils.CUSTOMER_1_ID, TestUtils.CUSTOMER_1_NAME, TestUtils.CUSTOMER_1_PHONE);

        List<CustomerResponse> customersResponse = getCustomers();

        // verifying if the list contains one customer
        assertEquals(customersResponse.size(), 1);

        // verifying if the new Customer is present
        assertCustomerEquals(customersResponse.stream()
                .filter(c -> TestUtils.CUSTOMER_1_ID.equals(c.getId()))
                .findAny()
                .orElse(new CustomerResponse()),
                newCustomer1);

    }

    /**
     * GIVEN there exists two {@link Customer}
     * WHEN a request for the list of all {@link Customer}
     * THEN a list containing two {@link Customer} should be returned.
     */
    @Test
    @Transactional
    public void shouldReturnListWithTwoCustomers() {

        checkIfNoCustomer();

        Customer newCustomer1 = createNewCustomer(TestUtils.CUSTOMER_1_ID, TestUtils.CUSTOMER_1_NAME, TestUtils.CUSTOMER_1_PHONE);
        Customer newCustomer2 = createNewCustomer(TestUtils.CUSTOMER_2_ID, TestUtils.CUSTOMER_2_NAME, TestUtils.CUSTOMER_2_PHONE);

        List<CustomerResponse> customersResponse = getCustomers();

        // verifying if the list contains two customer
        assertEquals(customersResponse.size(), 2);

        // verifying if the new Customers are present
        assertCustomerEquals(customersResponse.stream()
                .filter(c -> TestUtils.CUSTOMER_1_ID.equals(c.getId()))
                .findAny()
                .orElse(new CustomerResponse()),
                newCustomer1);

        assertCustomerEquals(customersResponse.stream()
                .filter(c -> TestUtils.CUSTOMER_2_ID.equals(c.getId()))
                .findAny()
                .orElse(new CustomerResponse()),
                newCustomer2);
    }

    /**
     * GIVEN a list of valid and invalid {@link Customer#getPhone()} from {@link Country#CAMEROON}
     * WHEN it map the list to {@link PhoneResponse}
     * THEN it should validate all valid and invalid phone numbers correctly.
     */
    @Test
    @Transactional
    public void shouldValidateCameroonPhones() {

        PhoneResponse validPhone1 = phoneResponseMapper.map(TestUtils.PHONE_CAMEROON_1_VALID);
        PhoneResponse validPhone2 = phoneResponseMapper.map(TestUtils.PHONE_CAMEROON_1_VALID);
        PhoneResponse invalidPhone1 = phoneResponseMapper.map(TestUtils.PHONE_CAMEROON_1_INVALID);
        PhoneResponse invalidPhone2 = phoneResponseMapper.map(TestUtils.PHONE_CAMEROON_2_INVALID);
        PhoneResponse invalidPhone3 = phoneResponseMapper.map(TestUtils.PHONE_CAMEROON_3_INVALID);
        PhoneResponse invalidPhone4 = phoneResponseMapper.map(TestUtils.PHONE_CAMEROON_4_INVALID);
        PhoneResponse invalidPhone5 = phoneResponseMapper.map(TestUtils.PHONE_CAMEROON_5_INVALID);
        PhoneResponse invalidPhone6 = phoneResponseMapper.map(TestUtils.PHONE_CAMEROON_6_INVALID);

        assertThatPhoneIsValid(validPhone1);
        assertThatPhoneIsValid(validPhone2);

        assertThatPhoneIsInvalid(invalidPhone1);
        assertThatPhoneIsInvalid(invalidPhone2);
        assertThatPhoneIsInvalid(invalidPhone3);
        assertThatPhoneIsInvalid(invalidPhone4);
        assertThatPhoneIsInvalid(invalidPhone5);
        assertThatPhoneIsInvalid(invalidPhone6);
    }

    /**
     * GIVEN a list of valid and invalid {@link Customer#getPhone()} from {@link Country#ETHIOPIA}
     * WHEN it map the list to {@link PhoneResponse}
     * THEN it should validate all valid and invalid phone numbers correctly.
     */
    @Test
    @Transactional
    public void shouldValidateEthiopiaPhones() {

        PhoneResponse validPhone1 = phoneResponseMapper.map(TestUtils.PHONE_ETHIOPIA_1_VALID);
        PhoneResponse validPhone2 = phoneResponseMapper.map(TestUtils.PHONE_ETHIOPIA_2_VALID);
        PhoneResponse invalidPhone1 = phoneResponseMapper.map(TestUtils.PHONE_ETHIOPIA_1_INVALID);
        PhoneResponse invalidPhone2 = phoneResponseMapper.map(TestUtils.PHONE_ETHIOPIA_2_INVALID);
        PhoneResponse invalidPhone3 = phoneResponseMapper.map(TestUtils.PHONE_ETHIOPIA_3_INVALID);
        PhoneResponse invalidPhone4 = phoneResponseMapper.map(TestUtils.PHONE_ETHIOPIA_4_INVALID);
        PhoneResponse invalidPhone5 = phoneResponseMapper.map(TestUtils.PHONE_ETHIOPIA_5_INVALID);

        assertThatPhoneIsValid(validPhone1);
        assertThatPhoneIsValid(validPhone2);

        assertThatPhoneIsInvalid(invalidPhone1);
        assertThatPhoneIsInvalid(invalidPhone2);
        assertThatPhoneIsInvalid(invalidPhone3);
        assertThatPhoneIsInvalid(invalidPhone4);
        assertThatPhoneIsInvalid(invalidPhone5);
    }

    /**
     * GIVEN a list of valid and invalid {@link Customer#getPhone()} from {@link Country#MOROCCO}
     * WHEN it map the list to {@link PhoneResponse}
     * THEN it should validate all valid and invalid phone numbers correctly.
     */
    @Test
    @Transactional
    public void shouldValidateMoroccoPhones() {

        PhoneResponse validPhone1 = phoneResponseMapper.map(TestUtils.PHONE_MOROCCO_1_VALID);
        PhoneResponse validPhone2 = phoneResponseMapper.map(TestUtils.PHONE_MOROCCO_1_VALID);
        PhoneResponse invalidPhone1 = phoneResponseMapper.map(TestUtils.PHONE_MOROCCO_1_INVALID);
        PhoneResponse invalidPhone2 = phoneResponseMapper.map(TestUtils.PHONE_MOROCCO_1_INVALID);
        PhoneResponse invalidPhone3 = phoneResponseMapper.map(TestUtils.PHONE_MOROCCO_1_INVALID);
        PhoneResponse invalidPhone4 = phoneResponseMapper.map(TestUtils.PHONE_MOROCCO_1_INVALID);

        assertThatPhoneIsValid(validPhone1);
        assertThatPhoneIsValid(validPhone2);

        assertThatPhoneIsInvalid(invalidPhone1);
        assertThatPhoneIsInvalid(invalidPhone2);
        assertThatPhoneIsInvalid(invalidPhone3);
        assertThatPhoneIsInvalid(invalidPhone4);
    }

    /**
     * GIVEN a list of valid and invalid {@link Customer#getPhone()} from {@link Country#MOZAMBIQUE}
     * WHEN it map the list to {@link PhoneResponse}
     * THEN it should validate all valid and invalid phone numbers correctly.
     */
    @Test
    @Transactional
    public void shouldValidateMozambiquePhones() {

        PhoneResponse validPhone1 = phoneResponseMapper.map(TestUtils.PHONE_MOZAMBIQUE_1_VALID);
        PhoneResponse validPhone2 = phoneResponseMapper.map(TestUtils.PHONE_MOZAMBIQUE_1_VALID);
        PhoneResponse validPhone3 = phoneResponseMapper.map(TestUtils.PHONE_MOZAMBIQUE_1_VALID);
        PhoneResponse validPhone4 = phoneResponseMapper.map(TestUtils.PHONE_MOZAMBIQUE_1_VALID);
        PhoneResponse invalidPhone1 = phoneResponseMapper.map(TestUtils.PHONE_MOZAMBIQUE_1_INVALID);
        PhoneResponse invalidPhone2 = phoneResponseMapper.map(TestUtils.PHONE_MOZAMBIQUE_1_INVALID);
        PhoneResponse invalidPhone3 = phoneResponseMapper.map(TestUtils.PHONE_MOZAMBIQUE_1_INVALID);
        PhoneResponse invalidPhone4 = phoneResponseMapper.map(TestUtils.PHONE_MOZAMBIQUE_1_INVALID);
        PhoneResponse invalidPhone5 = phoneResponseMapper.map(TestUtils.PHONE_MOZAMBIQUE_1_INVALID);

        assertThatPhoneIsValid(validPhone1);
        assertThatPhoneIsValid(validPhone2);
        assertThatPhoneIsValid(validPhone3);
        assertThatPhoneIsValid(validPhone4);

        assertThatPhoneIsInvalid(invalidPhone1);
        assertThatPhoneIsInvalid(invalidPhone2);
        assertThatPhoneIsInvalid(invalidPhone3);
        assertThatPhoneIsInvalid(invalidPhone4);
        assertThatPhoneIsInvalid(invalidPhone5);
    }

    /**
     * GIVEN a list of valid and invalid {@link Customer#getPhone()} from {@link Country#UGANDA}
     * WHEN it map the list to {@link PhoneResponse}
     * THEN it should validate all valid and invalid phone numbers correctly.
     */
    @Test
    @Transactional
    public void shouldValidateUgandaPhones() {

        PhoneResponse validPhone1 = phoneResponseMapper.map(TestUtils.PHONE_UGANDA_1_VALID);
        PhoneResponse invalidPhone1 = phoneResponseMapper.map(TestUtils.PHONE_UGANDA_1_INVALID);
        PhoneResponse invalidPhone2 = phoneResponseMapper.map(TestUtils.PHONE_UGANDA_2_INVALID);

        assertThatPhoneIsValid(validPhone1);

        assertThatPhoneIsInvalid(invalidPhone1);
        assertThatPhoneIsInvalid(invalidPhone2);
    }

    private void deleteAllCustomers() {
        customerRepository.deleteAll();
        customerRepository.flush();
    }

    @SuppressWarnings("unchecked")
    private List<CustomerResponse> getCustomers() {
        ResponseEntity<?> responseEntity = customerController.getCustomers();
        List<CustomerResponse> customersResponse = (List<CustomerResponse>) responseEntity.getBody();
        return customersResponse;
    }

    private void checkIfNoCustomer() {
        List<CustomerResponse> customersResponse = getCustomers();

        // verifying if the list is empty
        assertEquals(customersResponse.size(), 0);
    }

    private Customer createNewCustomer(Integer id, String name, String phone) {
        Customer customer = new Customer();
        customer.setId(id);
        customer.setName(name);
        customer.setPhone(phone);

        customerRepository.save(customer);
        customerRepository.flush();

        return customer;
    }

    private void assertCustomerEquals(CustomerResponse customerResponse, Customer customer2) {
        assertEquals(customerResponse.getId(), customer2.getId());
        assertEquals(customerResponse.getName(), customer2.getName());
        assertEquals(customerResponse.getPhone().getNumber(), customer2.getPhone());
    }

    private void assertThatPhoneIsValid(PhoneResponse validPhone) {
        assertNotEquals(validPhone.getNumber(), "");
        assertNotEquals(validPhone.getCountryName(), "");
        assertNotEquals(validPhone.getCountryCode(), "");
        assertTrue(validPhone.getState());
    }

    private void assertThatPhoneIsInvalid(PhoneResponse validPhone) {
        assertFalse(validPhone.getState());
    }

}

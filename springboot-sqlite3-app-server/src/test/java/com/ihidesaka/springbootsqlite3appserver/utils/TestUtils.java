package com.ihidesaka.springbootsqlite3appserver.utils;

public class TestUtils {

    // CUSTOMERS
    public final static Integer CUSTOMER_1_ID = 1;
    public final static String CUSTOMER_1_NAME = "Rei Ayanami";
    public final static String CUSTOMER_1_PHONE = "(55) 23680123";

    public final static Integer CUSTOMER_2_ID = 2;
    public final static String CUSTOMER_2_NAME = "Asuka Langley Soryu";
    public final static String CUSTOMER_2_PHONE = "237 23680123";

    //PHONES
    // Country CAMEROON
    public final static String PHONE_CAMEROON_1_VALID = "(237) 23680123";
    public final static String PHONE_CAMEROON_2_VALID = "(237) 236801234";
    
    public final static String PHONE_CAMEROON_1_INVALID = "(237) 2368012345";
    public final static String PHONE_CAMEROON_2_INVALID = "(237) 2368012";
    public final static String PHONE_CAMEROON_3_INVALID = "(237) 012345";
    public final static String PHONE_CAMEROON_4_INVALID = "(237) 0123456";
    public final static String PHONE_CAMEROON_5_INVALID = "(237) 01234567";
    public final static String PHONE_CAMEROON_6_INVALID = "(237) 012345678";

    // Country ETHIOPIA
    public final static String PHONE_ETHIOPIA_1_VALID = "(251) 590123456";
    public final static String PHONE_ETHIOPIA_2_VALID = "(251) 101234567";
    
    public final static String PHONE_ETHIOPIA_1_INVALID = "(251) 600123456";
    public final static String PHONE_ETHIOPIA_2_INVALID = "(251) 10123456";
    public final static String PHONE_ETHIOPIA_3_INVALID = "(251) 1012345678";
    public final static String PHONE_ETHIOPIA_4_INVALID = "(251) 59012345";
    public final static String PHONE_ETHIOPIA_5_INVALID = "(251) 5901234567";

    // Country MOROCCO
    public final static String PHONE_MOROCCO_1_VALID = "(212) 501234567";
    public final static String PHONE_MOROCCO_2_VALID = "(212) 901234567";
    
    public final static String PHONE_MOROCCO_1_INVALID = "(212) 401234567";
    public final static String PHONE_MOROCCO_2_INVALID = "(212) 100123456";
    public final static String PHONE_MOROCCO_3_INVALID = "(212) 50123456";
    public final static String PHONE_MOROCCO_4_INVALID = "(212) 5012345678";

    // Country MOZAMBIQUE
    public final static String PHONE_MOZAMBIQUE_1_VALID = "(258) 28012345";
    public final static String PHONE_MOZAMBIQUE_2_VALID = "(258) 280123456";
    public final static String PHONE_MOZAMBIQUE_3_VALID = "(258) 01234567";
    public final static String PHONE_MOZAMBIQUE_4_VALID = "(258) 01234567";
    
    public final static String PHONE_MOZAMBIQUE_1_INVALID = "(258) 2801234567";
    public final static String PHONE_MOZAMBIQUE_2_INVALID = "(258) 2801234";
    public final static String PHONE_MOZAMBIQUE_3_INVALID = "(258) 2801234567";
    public final static String PHONE_MOZAMBIQUE_4_INVALID = "(258) 012345";
    public final static String PHONE_MOZAMBIQUE_5_INVALID = "(258) 012345678";

    // Country UGANDA
    public final static String PHONE_UGANDA_1_VALID = "(256) 012345678";
    public final static String PHONE_UGANDA_1_INVALID = "(256) 01234567";
    public final static String PHONE_UGANDA_2_INVALID = "(256) 0123456789";

}

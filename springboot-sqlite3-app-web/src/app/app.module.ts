import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CustomerService } from './services/customer.service';
import { CountryService } from './services/country.service';
import { ENVIRONMENT } from './environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomerTableComponent } from './components/customer-table/customer-table.component';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';

// External references
import { DataTableModule } from "angular-6-datatable";
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
   declarations: [
      AppComponent,
      CustomerTableComponent,
      LayoutComponent,
      HeaderComponent,
      FooterComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      DataTableModule,
      FormsModule,
      NgSelectModule
   ],
   providers: [
      CountryService,
      CustomerService,
      { provide: 'BASE_API_URL', useValue: ENVIRONMENT.api }
  ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }

import { CountryService } from './../../services/country.service';
import { Component, OnInit } from '@angular/core';
import { Customer } from './../../models/customer.model';
import { CustomerService } from './../../services/customer.service';

@Component({
  selector: 'app-customer-table',
  templateUrl: './customer-table.component.html',
  styleUrls: ['./customer-table.component.sass']
})
export class CustomerTableComponent implements OnInit {

  private _customerList: Customer[] = [];
  private _filteredCustomerList: Customer[] = [];

  private _countryList: string[] = [];
  public stateList: boolean[] = [true, false];

  public phoneValidState: boolean = false;
  public selectedCountry: string[] = [];
  public selectedState: boolean;

  constructor(
    private countryService: CountryService,
    private customerService: CustomerService
  ) { }

  ngOnInit() {
    this.loadCountryList();
    this.loadCustomerList();
  }

  public get customerList() {
    return this._customerList;
  }

  public set customerList(customerList) {
    this._customerList = customerList;
  }

  public get countryList() {
    return this._countryList;
  }

  public set countryList(countryList) {
    this._countryList = countryList;
  }

  public get filteredCustomerList() {
    //this._filteredcustomerList;
    return this.getFilteredCustomerList();
  }

  public get isCustomerListEmpty() {
    return this.filteredCustomerList.length === 0 ? true : false;
  }

  private loadCountryList() {
    this.countryService.getCountries().subscribe((e) => {
      this._countryList = [];
      e.forEach(country => {
        this.countryList.push(country.name);
      });
    });
  }

  private loadCustomerList() {
    this.customerService.getCustomers().subscribe((e) => {
      this.customerList = [];
      e.forEach(customer => {
        this.customerList.push(customer);
      });
    });
  }

  private getFilteredCustomerList() {
    return this.customerList.filter(customer => {

      if (this.selectedCountry.length > 0) {
        if (this.selectedCountry.filter((country) => country === customer.phone.countryName).length === 0) {
          return false;
        }
      }

      if (this.selectedState != null) {
        if (this.selectedState !== customer.phone.state) {
          return false;
        }
      }

      return true;
    });
  }

}

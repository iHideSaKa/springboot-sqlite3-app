import { Country } from './country.model';

export class CountryClass implements Country {
  constructor (country: Country) {
    this.name = country.name;
  }

  name: string;
}

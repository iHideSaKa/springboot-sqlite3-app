import { PhoneClass } from './phone-class.model';
import { Customer } from './customer.model';

export class CustomerClass implements Customer {
  constructor (customer: Customer) {
    this.id = customer.id;
    this.name = customer.name;
    this.phone = new PhoneClass(customer.phone);
  }

  id: string;
  name: string;
  phone: PhoneClass;
}

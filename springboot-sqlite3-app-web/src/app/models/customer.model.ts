import { Phone } from './phone.model';

export interface Customer {
  id: string;
  name: string;
  phone: Phone;
}

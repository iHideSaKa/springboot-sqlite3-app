import { Phone } from './phone.model';

export class PhoneClass implements Phone {
  constructor (phone: Phone) {
    this.number = phone.number;
    this.countryName = phone.countryName;
    this.countryCode = phone.countryCode;
    this.state = phone.state;
  }

  number: string;
  countryName: string;
  countryCode: string;
  state: boolean;
}

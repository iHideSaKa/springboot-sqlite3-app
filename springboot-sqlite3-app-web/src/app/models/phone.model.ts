export interface Phone {
  number: string;
  countryName: string;
  countryCode: string;
  state: boolean;
}

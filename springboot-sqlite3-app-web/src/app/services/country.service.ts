import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Country } from './../models/country.model';
import { CountryClass } from '../models/country-class.model';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json ; charset=utf-8' })
};

const api = '/countries';

@Injectable()
export class CountryService {

  constructor(private http: HttpClient, @Inject('BASE_API_URL') private apiUrl: string) { }

  public getCountries() {
    return this.http.get<Country[]>(this.apiUrl + api, httpOptions)
      .pipe(map(c => c.map(c1 => new CountryClass(c1))));
  }

}

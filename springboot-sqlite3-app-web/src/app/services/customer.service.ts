import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Customer } from './../models/customer.model';
import { CustomerClass } from '../models/customer-class.model';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json ; charset=utf-8' })
};

const api = '/customers';

@Injectable()
export class CustomerService {

  constructor(private http: HttpClient, @Inject('BASE_API_URL') private apiUrl: string) { }

  public getCustomers() {
    return this.http.get<Customer[]>(this.apiUrl + api, httpOptions)
      .pipe(map(c => c.map(c1 => new CustomerClass(c1))));
  }

}
